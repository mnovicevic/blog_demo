<?php
/**
 * Created by PhpStorm.
 * User: milosnovicevic
 * Date: 9/23/14
 * Time: 1:31 AM
 */

class PostsController extends AppController
{
/**
 * @var array
 */
	public $components = array('Paginator');

	public function index()
	{
		$this->set('title_for_layout', 'Blogs');

		$this->paginate = array(
			'limit' => 3,
			'order' => array(
				'Post.created' => 'desc'
			)
		);
		$posts = $this->paginate('Post');
		foreach($posts as &$post) {
			$post['Post']['body'] = strip_tags($post['Post']['body']);
		}
		$this->set(compact('posts'));
	}

	public function view($id = null)
	{
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->Post->id = $id;
		$post = $this->Post->read();
		if (!$post) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->set('title_for_layout', $post['Post']['title']);
		$this->set('post', $post);
	}

	public function admin_index() {
		$this->Post->recursive = 0;
		$this->paginate = array(
			'limit' => 10,
			'order' => array(
				'Post.created' => 'desc'
			)
		);
		$this->set('posts', $this->paginate());
	}

/**
* admin_delete method
*
* @param string $id
* @return void
* @throws MethodNotAllowedException if request method is not post
* @throws NotFoundException If post doesn't exist
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		if ($this->Post->delete()) {
			$this->Session->setFlash(__('Post deleted!'));
			$this->redirect(array('action' => 'admin_index'));
		}
		$this->Session->setFlash(__('Post was not deleted'));
		$this->redirect(array('action' => 'admin_index'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 * @throws NotFoundException If post doesn't exist
 */
	public function admin_edit($id = null) {
		if (!$id) {
			throw new NotFoundException(__('Invalid post'));
		}

		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->request->data['Post']['image']['name'])) {
				var_dump($this->request->data);
				$file = $this->request->data['Post']['image'];
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$arr_ext = array('jpg', 'jpeg', 'gif');
				if(in_array($ext, $arr_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/uploads/posts/' . $file['name']);
					$this->request->data['Post']['image'] = $file['name'];
				}
			} else {
				unset($this->request->data['Post']['image']);
			}

			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('The article has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Post->read(null, $id);
		}
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Post->create();
			if(!empty($this->request->data['Post']['image']['name'])) {
				$file = $this->request->data['Post']['image'];
				$ext = substr(strtolower(strrchr($file['name'], '.')), 1);
				$arr_ext = array('jpg', 'jpeg', 'gif');
				if(in_array($ext, $arr_ext)) {
					move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/uploads/posts/' . $file['name']);
					$this->request->data['Post']['image'] = $file['name'];
				}
			}
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('Post has been created'));
				$this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->setFlash(__('Post could not be saved. Please, try again.'));
			}
		}

	}
}