<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 */
class Post extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'between' => array(
                'rule'    => array('between', 5, 20),
                'message' => 'Between 5 to 20 characters'
            )
		),
	);
}
