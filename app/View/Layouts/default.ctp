<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap-responsive');
		echo $this->Html->css('blog');

		echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"');
		echo $this->Html->script('bootstrap');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
          <a class="blog-nav-item active" href="/">Home</a>
        </nav>
      </div>
    </div>

	<div class="container">
        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>

		<div id="footer">

		</div>
	</div>
	<div class="blog-footer">
        <p>Blog template demo</p>
        <p><a href="#">Back to top</a></p>
    </div>

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
