<div>
    <?=$this->Form->create('Post',array(
            'class'=>'form-horizontal',
            'enctype' => 'multipart/form-data',
            'inputDefaults' => array('label'=>false, 'div'=>false, 'class'=>'form-control')
        )); ?>
        <fieldset>
            <legend><?php echo __('Admin Edit Post'); ?></legend>
            <?=$this->Form->input('Post.id');?>

            <div class="form-group">
                <label class="col-sm-2 control-label">Author</label>
                <div class="col-sm-10">
                    <?=$this->Form->input('Post.author');?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Title</label>
                <div class="col-sm-10">
                    <?=$this->Form->input('Post.title');?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Text</label>
                <div class="col-sm-10">
                    <?=$this->Form->input('Post.body');?>
                </div>
            </div>
            <? if ($this->Form->value('Post.image')): ?>
                <?=$this->Html->image('/img/uploads/posts/'.$this->Form->value('Post.image'), array('alt' => 'CakePHP', 'width' => '100px'));?>
            <? endif; ?>

            <div class="form-group">
                <label class="col-sm-2 control-label">Image</label>
                <div class="col-sm-10">
                    <?=$this->Form->input('Post.image', array('type'=>'file'));?>
                </div>
            </div>
        </fieldset>
    <?=$this->Form->submit('Save', array('class' => 'btn btn-primary'));?>
    <?=$this->Form->end();?>
</div>
