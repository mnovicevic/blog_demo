<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Author</th>
                <th>Image</th>
                <th>Created</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post): ?>
                <tr>
                    <td><? echo $post['Post']['id'] ?></td>
                    <td><?php echo $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'edit', $post['Post']['id'])); ?></td>
                    <td><? echo $post['Post']['author'] ?></td>
                    <td><? echo $post['Post']['image'] ?></td>
                    <td><? echo $post['Post']['created'] ?></td>
                    <td>
                        <?php echo $this->Html->link('Edit', array('action' => 'admin_edit', $post['Post']['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'admin_delete', $post['Post']['id']), null, __('Are you sure you want to delete post with id: #%s?', $post['Post']['id'])); ?></li>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php echo $this->Html->link('NEW POST', array('action' => 'admin_add'), array('class' => 'btn btn-primary btn-lg')); ?>