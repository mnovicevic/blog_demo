<div class="blog-header">
    <h1 class="blog-title">The Bootstrap Blog</h1>
    <p class="lead blog-description">Blog demo description</p>
</div>

<div class="row">

    <div class="col-sm-10 blog-main">
        <?php foreach ($posts as $post): ?>
            <div class="blog-post">
                <h2 class="blog-post-title">
                    <?=$this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
                </h2>
                <p class="blog-post-meta"><?=$post['Post']['created']; ?> by <?=$post['Post']['author']; ?></p>
                <? if (!empty($post['Post']['image'])): ?>
                    <div>
                        <?=$this->Html->image('/img/uploads/posts/'.$post['Post']['image'], array('alt' => '', 'width' => '120px', 'style' => 'float: right'));?>
                    </div>
                <? endif; ?>
                <?= $this->Text->truncate($post['Post']['body'], 550, array('ending' => '...', 'exact' => false)) ?>
            </div>
            <div style="clear: both;"></div>
        <?php endforeach; ?>
        <ul class="pager">
            <?php if($this->Paginator->hasPrev()): ?>
            <li><?=$this->Paginator->prev( __('previous', true), array('tag' => false), null, array('class' => 'disabled'));?></li>
            <?php endif; ?>
            <?php if($this->Paginator->hasNext()): ?>
            <li><?=$this->Paginator->next(__('Next', true), array('tag' => false), null, array('class' => 'disabled'));?></li>
            <?php endif; ?>
        </ul>
    </div>
</div>