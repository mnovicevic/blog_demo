<div class="blog-header">
    <h1 class="blog-title">The Bootstrap Blog</h1>
    <p class="lead blog-description">Blogs description</p>
</div>

<div class="row">

    <div class="col-sm-8 blog-main">
        <div class="blog-post">
            <h2 class="blog-post-title">
                <?php echo $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
            </h2>
            <p class="blog-post-meta"><?php echo $post['Post']['created']; ?> by <?php echo $post['Post']['author']; ?></p>
            <? if (!empty($post['Post']['image'])): ?>
            <div style="float: right;">
                <?=$this->Html->image('/img/uploads/posts/'.$post['Post']['image'], array('alt' => 'CakePHP', 'width' => '250px'));?>
            </div>
            <? endif; ?>

            <?= $post['Post']['body'] ?>
        </div>
    </div>
</div>